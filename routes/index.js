var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/greetings', function(req, res, next) {
    // disable Chrome XSS protection to allow demo to work
    res.set("X-XSS-Protection","0");
    res.render('greetings', { user: req.body.name });

});

router.post('/uploads', function(req, res, next) {
  if (!req.files || Object.keys(req.files).length === 0) {
    return res.status(400).send('No files were uploaded.')
  }

  let sampleFile = req.files.sampleFile;
  let uploadPath = '/tmp/'+sampleFile.name;

  sampleFile.mv(uploadPath, function(err) {
    if (err) {
      return res.status(500).send(err)
    }

    res.send('File uploaded to ' + uploadPath + ' !')
  })
});

module.exports = router;
